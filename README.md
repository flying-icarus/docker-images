# Collection of aviation related docker images

Details about each image are contained in the respective folders.

They can be composed together in a docker file like so:

```yaml
version: "3.3"
services:
  rtlsdr_airband:
    image: flyingicarus/rtlsdr-airband-rpiv2-arm32v6
    container_name: rtlsdr_airband
    restart: always
    privileged: true
    volumes:
      - ${PWD}/rtl_airband.conf:/rtl_airband.conf
      - ${PWD}/atc-archives:/archives
  dump1090:
    image: flyingicarus/dump1090-fa-arm32v6
    container_name: dump1090
    restart: always
    privileged: true
    command:
      dump1090-fa
        --quiet
        --net
        --device 00000004
        --lat xx.xxx
        --lon xx.xxx
        --modeac
        --fix
  flightaware_feeder:
    image: flyingicarus/piaware-arm32v6
    container_name: flightaware_feeder
    restart: always
    depends_on:
      - dump1090
    volumes:
      - ./piaware.conf:/etc/piaware.conf
  opensky_feeder:
    image: flyingicarus/opensky-feeder-arm32v6
    container_name: opensky_feeder
    restart: always
    depends_on:
      - dump1090
    volumes:
      - ./opensky.conf:/etc/openskyd/conf.d/10-custom.conf
  flightradar24_feeder:
    image: flyingicarus/fr24feed-arm32v6
    container_name: flightradar24_feeder
    restart: always
    depends_on:
      - dump1090
    volumes:
      - ./fr24feed.ini:/etc/fr24feed.ini
  radarbox_feeder:
    image: flyingicarus/rbfeeder-arm32v6
    container_name: radarbox_feeder
    restart: always
    depends_on:
      - dump1090
    volumes:
      - ./rbfeeder.ini:/etc/rbfeeder.ini
  adsbexchange_feeder_beast:
    image: alpine:latest
    container_name: adsbexchange_feeder_beast
    restart: always
    tty: true
    depends_on:
      - dump1090
    command:
      sh -c 'nc dump1090 30005 | nc feed.adsbexchange.com 30005'
  adsbexchange_feeder_mlat:
    image: flyingicarus/mlat-client-arm32v6:latest
    container_name: adsbexchange_feeder_mlat
    restart: always
    depends_on:
      - dump1090
    command:
      mlat-client
        --input-type dump1090
        --input-connect dump1090:30005
        --lat xx.xxx
        --lon xx.xxx
        --alt xx
        --user xxx
        --server feed.adsbexchange.com:31090
        --no-udp
```
